<?php
header('Access-Control-Allow-Origin: prueba.cliteratu.re');
//header('Content-Type: application/json');

ini_set('display_errors', 1);
require_once('app_management.php');
require_once('TwitterAPIExchange.php');

// Your specific requirements
$url = 'https://api.twitter.com/1.1/search/tweets.json';
$requestMethod = 'GET';
$getfield = '?'.$_SERVER['QUERY_STRING'];

// Perform the request
$twitter = new TwitterAPIExchange($settings);

$api_response = $twitter -> setGetfield($getfield)
                         -> buildOauth($url, $requestMethod)
                         -> performRequest();

echo json_encode($api_response);

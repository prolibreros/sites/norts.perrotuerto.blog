# noRTs

Una pequeña aplicación de ejemplo donde se buscan *tweets* con
las siguientes características:

* Muestra los *tweets* más recientes **sin retweets**.
* Muestra los *tweets* en orden cronológico (el más antiguo primero).
* Indica la fecha de publicación precisa de cada *tweet*.
* Indica el lugar de pubicación de cada *tweet* (si lo hay).
* Menciona cuando hay nuevos *tweets*.
* Sugiere el *top* 5 de *trending topics*.
* No existe la necesidad de tener una cuenta de Twitter para ver el contenido.

La idea general es ayudar a filtrar el mar de información,
principalmente en noticias de último momento.

> Ojo: la localización es únicamente para mostrar los *trends* del
> país donde se encuentra el usuario.

# ¿A mejorar?

* ¿Qué más? No sé, digan.

# Configuración para el fork

1. Colocar todo en un servidor.
2. Renombrar el archivo `app_management_empyy.php` presente en `php` por `app_management.php`.
3. Añadir las credenciales necesarias de [Twitter Apps](https://apps.twitter.com/).
4. ¡Listo!

# Licencia

La aplicación noRTs está bajo licencia GPL v3.

Un agradecimiento a [J7mbo](https://github.com/J7mbo/twitter-api-php)
por tener disponible una API de ejemplo de Twitter.
